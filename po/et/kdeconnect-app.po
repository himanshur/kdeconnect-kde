# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeconnect-kde package.
#
# Marek Laane <qiilaq69@gmail.com>, 2019.
# Mihkel Tõnnov <mihhkel@gmail.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-kde\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-16 00:45+0000\n"
"PO-Revision-Date: 2020-11-20 01:14+0100\n"
"Last-Translator: Mihkel Tõnnov <mihhkel@gmail.com>\n"
"Language-Team: Estonian <>\n"
"Language: et\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.3\n"

#: main.cpp:32 main.cpp:34
#, kde-format
msgid "KDE Connect"
msgstr "KDE Connect"

#: main.cpp:36
#, kde-format
msgid "(c) 2015, Aleix Pol Gonzalez"
msgstr "(c) 2015: Aleix Pol Gonzalez"

#: main.cpp:37
#, kde-format
msgid "Aleix Pol Gonzalez"
msgstr "Aleix Pol Gonzalez"

#: main.cpp:37
#, kde-format
msgid "Maintainer"
msgstr "Hooldaja"

#: main.cpp:38
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Marek Laane"

#: main.cpp:38
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "qiilaq69@gmail.com"

#: main.cpp:54
#, kde-format
msgid "URL to share"
msgstr ""

#: qml/DevicePage.qml:23
#, kde-format
msgid "Unpair"
msgstr "Lahuta paaristus"

#: qml/DevicePage.qml:28
#, kde-format
msgid "Send Ping"
msgstr "Saada ping"

#: qml/DevicePage.qml:36 qml/PluginSettings.qml:15
#, kde-format
msgid "Plugin Settings"
msgstr "Plugina seadistused"

#: qml/DevicePage.qml:60
#, kde-format
msgid "Multimedia control"
msgstr "Multimeedia juhtimine"

#: qml/DevicePage.qml:67
#, kde-format
msgid "Remote input"
msgstr "Kaugsisend"

#: qml/DevicePage.qml:74 qml/presentationRemote.qml:15
#, kde-format
msgid "Presentation Remote"
msgstr "Kaugesitlus"

#: qml/DevicePage.qml:83 qml/mousepad.qml:44
#, kde-format
msgid "Lock"
msgstr "Lukusta"

#: qml/DevicePage.qml:83
#, kde-format
msgid "Unlock"
msgstr "Eemalda lukustus"

#: qml/DevicePage.qml:90
#, kde-format
msgid "Find Device"
msgstr "Otsi seadet"

#: qml/DevicePage.qml:95 qml/runcommand.qml:16
#, kde-format
msgid "Run command"
msgstr "Käivita käsk"

#: qml/DevicePage.qml:104
#, kde-format
msgid "Send Clipboard"
msgstr ""

#: qml/DevicePage.qml:110
#, kde-format
msgid "Share File"
msgstr "Jaga faili"

#: qml/DevicePage.qml:115 qml/volume.qml:16
#, kde-format
msgid "Volume control"
msgstr "Helitugevuse juhtimine"

#: qml/DevicePage.qml:124
#, kde-format
msgid "This device is not paired"
msgstr "See seade on paaristamata"

#: qml/DevicePage.qml:128
#, fuzzy, kde-format
#| msgid "Pair"
msgctxt "Request pairing with a given device"
msgid "Pair"
msgstr "Paaristu"

#: qml/DevicePage.qml:135 qml/DevicePage.qml:144
#, kde-format
msgid "Pair requested"
msgstr "Paardumise soov"

#: qml/DevicePage.qml:150
#, kde-format
msgid "Accept"
msgstr "Nõustu"

#: qml/DevicePage.qml:156
#, kde-format
msgid "Reject"
msgstr "Lükka tagasi"

#: qml/DevicePage.qml:165
#, kde-format
msgid "This device is not reachable"
msgstr "See seade ei ole kättesaadav"

#: qml/DevicePage.qml:173
#, kde-format
msgid "Please choose a file"
msgstr "Palun vali fail"

#: qml/FindDevicesPage.qml:23
#, fuzzy, kde-format
#| msgid "Find Device"
msgctxt "Title of the page listing the devices"
msgid "Devices"
msgstr "Otsi seadet"

#: qml/FindDevicesPage.qml:38
#, kde-format
msgid "No devices found"
msgstr "Ühtegi seadet ei leitud"

#: qml/FindDevicesPage.qml:51
#, kde-format
msgid "Remembered"
msgstr "Meeldejäetud"

#: qml/FindDevicesPage.qml:53
#, kde-format
msgid "Available"
msgstr "Saadaval"

#: qml/FindDevicesPage.qml:55
#, kde-format
msgid "Connected"
msgstr "Ühendatud"

#: qml/main.qml:73
#, kde-format
msgid "Find devices..."
msgstr "Otsi seadmeid ..."

#: qml/main.qml:118 qml/main.qml:121
#, fuzzy, kde-format
#| msgid "Plugin Settings"
msgid "Settings"
msgstr "Plugina seadistused"

#: qml/mousepad.qml:16
#, kde-format
msgid "Remote Control"
msgstr "Kaugjuhtimine"

#: qml/mousepad.qml:59
#, kde-format
msgid "Press %1 or the left and right mouse buttons at the same time to unlock"
msgstr ""

#: qml/mpris.qml:19
#, kde-format
msgid "Multimedia Controls"
msgstr "Multimeedia juhtimine"

#: qml/mpris.qml:65
#, kde-format
msgid "No players available"
msgstr "Ühtegi mängijat pole saada"

#: qml/mpris.qml:105
#, kde-format
msgid "%1 - %2"
msgstr "%1 - %2"

#: qml/presentationRemote.qml:20
#, kde-format
msgid "Enable Full-Screen"
msgstr "Luba täisekraan"

#: qml/runcommand.qml:21
#, kde-format
msgid "Edit commands"
msgstr "Muuda käske"

#: qml/runcommand.qml:24
#, kde-format
msgid "You can edit commands on the connected device"
msgstr "Ühendatud seadmes saab käske muuta"

#: qml/runcommand.qml:43
#, kde-format
msgid "No commands defined"
msgstr "Ühtegi käsku pole määratud"

#: qml/Settings.qml:13
#, fuzzy, kde-format
#| msgid "Plugin Settings"
msgctxt "@title:window"
msgid "Settings"
msgstr "Plugina seadistused"

#: qml/Settings.qml:21
#, kde-format
msgid "Device name"
msgstr ""

#: qml/Settings.qml:36
#, fuzzy, kde-format
#| msgid "KDE Connect"
msgid "About KDE Connect"
msgstr "KDE Connect"

#: qml/Settings.qml:47
#, fuzzy, kde-format
#| msgid "KDE Connect"
msgid "About KDE"
msgstr "KDE Connect"
